package com.startx.core.system.config;

/**
 * 项目配置类
 */
public class StartxConfig {
	/**
	 * 绑定的域名
	 */
	private String ip = "localhost";
	/**
	 * 监听端口
	 */
	private int port = 8080;
	/**
	 * boss线程数
	 */
	private int boss = 2;
	/**
	 * workder线程数
	 */
	private int worker = 4;
	/**
	 * spring配置路径
	 */
	private String springPath = "classpath*:application.xml";
	/**
	 * 项目访问前缀
	 */
	private String endPoint = "/";
	/**
	 * websocket访问链接
	 */
	private String websocket = "/websocket";
	/**
	 * 是否为ssl
	 */
	private boolean isSSL = false;
	/**
	 * 静态资源目录
	 */
	private String resource = "/resource";
	// 以下配置在ssl连接时使用
	/**
	 * jsk密码
	 */
	private String jksPwd;
	/**
	 * jsk路径
	 */
	private String jksPath;
	/**
	 * endpoint扫描路径
	 */
	private String packages = "com.startx";
	/**
	 * 设置NettyServer启动类
	 */
	private String serverClass = "com.startx.core.netty.server.impl.HttpServer";
	
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getSpringPath() {
		return springPath;
	}

	public void setSpringPath(String springPath) {
		this.springPath = springPath;
	}

	public String getEndPoint() {
		return endPoint;
	}

	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
	}

	public boolean isSSL() {
		return isSSL;
	}

	public void setSSL(boolean isSSL) {
		this.isSSL = isSSL;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public String getPackages() {
		return packages;
	}

	public void setPackages(String packages) {
		this.packages = packages;
	}

	public String getServerClass() {
		return serverClass;
	}

	public void setServerClass(String serverClass) {
		this.serverClass = serverClass;
	}

	public int getBoss() {
		return boss;
	}

	public void setBoss(int boss) {
		this.boss = boss;
	}

	public int getWorker() {
		return worker;
	}

	public void setWorker(int worker) {
		this.worker = worker;
	}

	public String getJksPwd() {
		return jksPwd;
	}

	public void setJksPwd(String jksPwd) {
		this.jksPwd = jksPwd;
	}

	public String getJksPath() {
		return jksPath;
	}

	public void setJksPath(String jksPath) {
		this.jksPath = jksPath;
	}

	public String getWebsocket() {
		return websocket;
	}

	public void setWebsocket(String websocket) {
		this.websocket = websocket;
	}

	@Override
	public String toString() {
		return "StartxConfig [ip=" + ip + ", port=" + port + ", boss=" + boss + ", worker=" + worker
				+ ", springPath=" + springPath + ", endPoint=" + endPoint + ", websocket=" + websocket + ", isSSL="
				+ isSSL + ", resource=" + resource + ", jksPwd=" + jksPwd + ", jksPath=" + jksPath + ", packages="
				+ packages + ", serverClass=" + serverClass + "]";
	}

}
