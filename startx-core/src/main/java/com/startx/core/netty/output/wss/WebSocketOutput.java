package com.startx.core.netty.output.wss;

import com.startx.core.system.factory.ChannelFactory;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

public abstract class WebSocketOutput extends SimpleChannelInboundHandler<Object> {
	
	 /**
     * 推送单个
     * */
    public static final void push(final ChannelHandlerContext ctx,final String message){
        TextWebSocketFrame tws = new TextWebSocketFrame(message);
        ctx.channel().writeAndFlush(tws);
    }
    
    /**
     * 推送单个
     * */
    public static final void push(String key,final String message){
        TextWebSocketFrame tws = new TextWebSocketFrame(message);
        ChannelHandlerContext ctx = ChannelFactory.getByKey(key);
        
        if(ctx != null && ctx.channel().isWritable()) {
        	ctx.channel().writeAndFlush(tws);
        } else {
        	throw new RuntimeException("网络连接中断");
        }
    }
    
    /**
     * 群发
     * 
     * */
    public static final void push(final String message){
        TextWebSocketFrame tws = new TextWebSocketFrame(message);
        ChannelFactory.getGroup().writeAndFlush(tws);
    }
    
    /**
     * 群发
     * 
     * */
    public static final void push(final ChannelGroup ctxGroup,final String message){
        TextWebSocketFrame tws = new TextWebSocketFrame(message);
        ctxGroup.writeAndFlush(tws);
    }
	
}
