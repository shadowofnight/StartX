package com.startx.core.mvc.define;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 方法注解
 */
@Target(value={ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface RequestMapping {
	String[] value();
	RequestMethod[] method() default {RequestMethod.GET};
	ResponseType type() default ResponseType.JSON;
}
