package com.startx.core;

import com.startx.core.config.reader.impl.PropertiesConfigReader;
import com.startx.core.mvc.AccessMapping;
import com.startx.core.netty.server.Server;

/**
 * 服务启动入口
 */
public class Bootstrap {
	
	/**
	 * 启动服务
	 */
	public static void start() throws Exception {
		// 初始化配置文件
		config();
		// 初始化AccessPoint，初始化Spring
		AccessMapping.start();
		// 启动NettyServer
		Server.start();
	}
	
	/**
	 * 读取配置
	 */
	private static void config() throws Exception {
		new PropertiesConfigReader().read();
	}
}
