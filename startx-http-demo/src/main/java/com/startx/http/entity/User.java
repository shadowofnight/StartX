package com.startx.http.entity;

import java.util.List;
import java.util.Map;

public class User {
	
	private int id;
	private String realname;
	private String password;
	private int    age;
	private Map<String,Object> plugin;
	private List<String> son;
	
	public User(int id, String realname, String password, int age,Map<String,Object> plugin,List<String> son) {
		super();
		this.id = id;
		this.realname = realname;
		this.password = password;
		this.age = age;
		this.plugin = plugin;
		this.son = son;
	}

	public List<String> getSon() {
		return son;
	}

	public void setSon(List<String> son) {
		this.son = son;
	}

	public Map<String, Object> getPlugin() {
		return plugin;
	}

	public void setPlugin(Map<String, Object> plugin) {
		this.plugin = plugin;
	}



	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getRealname() {
		return realname;
	}
	
	public void setRealname(String realname) {
		this.realname = realname;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public int getAge() {
		return age;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
}
