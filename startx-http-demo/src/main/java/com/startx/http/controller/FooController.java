package com.startx.http.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.stereotype.Controller;

import com.startx.core.mvc.anotation.RequestMapping;
import com.startx.core.mvc.anotation.RequestMethod;
import com.startx.core.mvc.anotation.ResponseType;
import com.startx.core.system.param.HttpArgs;
import com.startx.http.entity.User;

import io.netty.channel.ChannelHandlerContext;

@Controller
@RequestMapping("/foo")
public class FooController {
	
	
	//com.startx.core.point.factory.PointFactory  - GET_/foo/bar_JSON
	@RequestMapping(value="/bar",method=RequestMethod.GET,type=ResponseType.JSON)
	public Object bar(byte[] body,HttpArgs args,ChannelHandlerContext ctx) {
		
		System.out.println(body.length);
		System.out.println(args.getHeaders());
		System.out.println(args.getParams());
		System.out.println(args.getIp());
		System.out.println(args.getDomain());
		
		//ctx.write()
		
		Map<String,Object> values = new HashMap<>();
		
		values.put("timestamp", System.currentTimeMillis());
		values.put("nonstr", UUID.randomUUID().toString());
		
		return values;
	}
	
	@RequestMapping(value="/map",method=RequestMethod.GET,type=ResponseType.XML)
	public Object map(Map<String,Object> params) {
		
		List<String> son = new ArrayList<>();
		son.add("jim");
		son.add("lucy");
		
		List<Object> vallist = new ArrayList<>();
		
		Map<String,Object> values = new HashMap<>();
		
		Map<String,Object> child = new HashMap<>();
		child.put("timestamp", System.currentTimeMillis());
		child.put("nonstr", UUID.randomUUID().toString());
		
		values.put("child", child);
		
		values.put("user", new User(1,"张三","123456",16, child,son));
		
		vallist.add(values);
		vallist.add(values);
		
		return vallist;
	}
	
	@RequestMapping(value="/json",method=RequestMethod.GET,type=ResponseType.JSON)
	public Object json(Map<String,Object> params) {
		
		List<String> son = new ArrayList<>();
		son.add("jim");
		son.add("lucy");
		
		List<Object> vallist = new ArrayList<>();
		
		Map<String,Object> values = new HashMap<>();
		
		Map<String,Object> child = new HashMap<>();
		child.put("timestamp", System.currentTimeMillis());
		child.put("nonstr", UUID.randomUUID().toString());
		
		values.put("child", child);
		
		values.put("user", new User(1,"张三","123456",16, child,son));
		
		vallist.add(values);
		vallist.add(values);
		
		return vallist;
	}
}
